<?php

namespace Lavalite\Page\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Traits\Slugger;
use Litepie\Database\Model;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Trans\Traits\Trans;
use Litepie\Revision\Traits\Revision;
use Quatius\Framework\Traits\ParamSetter;

class Page extends Model
{
    use Filer, SoftDeletes, Hashids, Slugger, Trans, Revision, ParamSetter;

    /**
     * Configuartion for the model.
     *
     * @var array
     */
     protected $config = 'package.page.page';
     
     // disable unique check
     protected function getSluggableUniqueAttributeValue($name, $value)
     {
         return $value;
     }

     private function getDefaultParams(){
        return 'banner';
    }
}
