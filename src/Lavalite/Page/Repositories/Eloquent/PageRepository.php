<?php

namespace Lavalite\Page\Repositories\Eloquent;

use Lavalite\Page\Interfaces\PageRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;
use Litepie\Repository\Traits\CacheableRepository;
use Quatius\Framework\Traits\CacheableUpdate;

class PageRepository extends BaseRepository implements PageRepositoryInterface
{
    use CacheableRepository, CacheableUpdate;
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        
        return config('package.page.page.model');
    }

    /**
     * Get page by id or slug.
     *
     * @return void
     */
    public function getPage($var)
    {
        if (!$this->allowedCache('getPage') || $this->isSkippedCache()) {
            return $this->findBySlug($var);
        }
        
        $key     = $this->getCacheKey('getPage', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value   = $this->getCacheRepository()->remember($key, $minutes, function () use ($var) {
            return $this->findBySlug($var);
        });
        
        return $value;
    }
}
