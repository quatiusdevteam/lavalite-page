<?php

// Admin routes for module
Route::group(['prefix' => trans_setlocale() . '/admin/page'], function () {
    Route::resource('page', 'PageAdminWebController');
});

// User routes for module
Route::group(['prefix' => trans_setlocale() . '/user/page'], function () {
    Route::resource('page', 'PageUserWebController');
});

// Public routes for module Override my admin modules
/* Route::group(['prefix' => trans_setlocale()], function () {
    Route::get('/{slug}'.config('package.page.suffix','.html'), 'PagePublicWebController@getPage');
}); */
